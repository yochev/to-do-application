﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoApplication.Models;

namespace ToDoApplication.EfDataAccess
{
    public class TodoRepository : Repository<Todo>
    {
        public List<Todo> GetAll(int categoryId)
        {
            return dbSet.Where(t => t.CategoryId == categoryId).ToList();
        }
    }
}