﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ToDoApplication.Models;

namespace ToDoApplication.EfDataAccess
{
    public abstract class Repository<T> where T : BaseModel
    {
        protected ToDoApplicationContext db;
        protected DbSet<T> dbSet;

        public Repository()
        {
            db = new ToDoApplicationContext();
            dbSet = db.Set<T>();
        }

        public virtual List<T> GetAll()
        {
            return dbSet.ToList();
        }

        public virtual T Get(int id)
        {
            return dbSet.Find(id);
        }

        protected virtual void Insert(T entity)
        {
            dbSet.Add(entity);
        }

        protected virtual void Update(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save(T entity)
        {
            if (entity.Id > 0)
            {
                Update(entity);
            }
            else
            {
                Insert(entity);
            }

            db.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
            db.SaveChanges();
        }
    }
}