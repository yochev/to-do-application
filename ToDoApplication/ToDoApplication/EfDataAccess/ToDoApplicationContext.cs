﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ToDoApplication.Models;

namespace ToDoApplication.EfDataAccess
{
    public class ToDoApplicationContext : DbContext
    {
        public ToDoApplicationContext()
            : base()
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Todo> Todos { get; set; }

    }
}