﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using ToDoApplication.EfDataAccess;
using ToDoApplication.Models;
using ToDoApplication.ViewModels.Category;

namespace ToDoApplication.Controllers
{
    public class CategoryController : Controller
    {
        //
        // GET: /Category/
        public ActionResult Index()
        {
            CategoryIndexViewModel model = new CategoryIndexViewModel();

            CategoryRepository repository = new CategoryRepository();

            model.CurrentDateTime = DateTime.Now;
            model.Categories = repository.GetAll();

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CategoryCreateViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CategoryRepository repository = new CategoryRepository();

            Category category = new Category();
            category.Id = model.Id;
            category.Name = model.Name;

            repository.Save(category);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            CategoryRepository repository = new CategoryRepository();

            Category category = repository.Get(id);

            if (category == null)
            {
                return HttpNotFound();
            }

            CategoryEditViewModel model = new CategoryEditViewModel();
            model.Id = category.Id;
            model.Name = category.Name;

            return View(model);            
        }

        [HttpPost]
        public ActionResult Edit(CategoryEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CategoryRepository repository = new CategoryRepository();

            Category category = repository.Get(model.Id);

            if (category == null)
            {
                return HttpNotFound();
            }

            category.Id = model.Id;
            category.Name = model.Name;

            repository.Save(category);


            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            CategoryRepository repository = new CategoryRepository();

            Category category = repository.Get(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            repository.Delete(category);

            return RedirectToAction("Index");
        }
	}
}