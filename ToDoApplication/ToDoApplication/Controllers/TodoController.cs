﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoApplication.DataAccess;
using ToDoApplication.Models;

namespace ToDoApplication.Controllers
{
    public class TodoController : Controller
    {
        private TodoRepository repository;

        public TodoController()
        {
            repository = new TodoRepository(ConfigurationManager.ConnectionStrings["ToDoApp"].ToString());
        }
        //
        // GET: /Todo/
        public ActionResult Index(int categoryId)
        {
            List<Todo> model = repository.GetByCategoryId(categoryId);

            return View(model);
        }

        public ActionResult Create(int categoryId)
        {
            Todo todo = new Todo();
            todo.CategoryId = categoryId;

            return View(todo);
        }

        [HttpPost]
        public ActionResult Create(Todo todo)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            repository.Insert(todo);
            return RedirectToAction("Index", new { categoryId = todo.CategoryId});
        }
	}
}