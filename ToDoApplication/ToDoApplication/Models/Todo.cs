﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoApplication.Models
{
    public class Todo : BaseModel
    {
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }

        public virtual Category Category { get; set; }
    }
}