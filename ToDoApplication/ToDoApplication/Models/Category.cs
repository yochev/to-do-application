﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoApplication.Models
{
    public class Category : BaseModel
    {
        public string Name { get; set; }

        public virtual List<Todo> Todoes { get; set; }
    }
}