﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ToDoApplication.ViewModels.Category
{
    public class CategoryEditViewModel
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "Name should contain letters only")]
        public string Name { get; set; }

    }
}